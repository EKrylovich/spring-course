package spring.course.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class BookUserDetailsService implements UserDetailsService {

    private static final Map<String, UserDetails> userStorage = new HashMap<>();

    static {
        userStorage.put("user", new BookUser("user",
                "password",
                Collections.singletonList(new SimpleGrantedAuthority("USER"))));


        userStorage.put("admin", new BookUser("admin",
                "password",
                Arrays.asList(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN"))));
    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userStorage.getOrDefault(username, new DeniedUser());
    }

}
