package spring.course.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

                                //For JDBC authentication
//    @Autowired
    private DataSource dataSource;

    @Autowired
    private BookUserDetailsService bookUserDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("memory_user").password("password").roles("USER").and()
                .withUser("memory_admin").password("password").roles("USER", "ADMIN");

                                //For JDBC authentication
//        auth
//                .jdbcAuthentication()
//                .dataSource(dataSource)
//                .usersByUsernameQuery("select username, password, true from book_users where username = ?")
//                .passwordEncoder(new StandardPasswordEncoder("53cr3t"));

        auth.userDetailsService(bookUserDetailsService);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/addBook").hasAuthority("ADMIN")
                .antMatchers("/editBook/**").hasAuthority("ADMIN")
                .antMatchers("/deleteBook/**").hasAuthority("ADMIN")
                .antMatchers("/home").permitAll()
                .antMatchers("/error").permitAll()
                .antMatchers("/viewBooks").hasAuthority("USER")
//                .anyRequest().authenticated()
        .and()
                .formLogin()
        .and()
                .httpBasic()
                    .realmName("bookShop")
        .and()
                .rememberMe()
                .tokenValiditySeconds(2419200)
                .key("bookShopKey")
        .and()
                .logout()
                .logoutSuccessUrl("/");
    }
}
