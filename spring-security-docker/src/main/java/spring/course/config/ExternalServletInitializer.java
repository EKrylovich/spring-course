package spring.course.config;

import org.springframework.web.WebApplicationInitializer;
import spring.course.servlet.ExternalServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class ExternalServletInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        ServletRegistration.Dynamic myServlet = servletContext.addServlet("mySuperServlet", ExternalServlet.class);
        myServlet.addMapping("/myCustomSuperExternalLink/**");
    }
}
