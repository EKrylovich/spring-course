package spring.course.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import spring.course.book.controller.BookController;

@ControllerAdvice(assignableTypes = {BookController.class})
public class ExceptionHandlers {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleCannotDeleteException(Exception exp){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("errorMessage", exp.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }


}
