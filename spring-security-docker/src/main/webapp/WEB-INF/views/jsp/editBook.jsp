<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>



<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Edit Book</h1>
<form:form method="POST" action="/spring-security/editBook" commandName="book">
     <table >
         <tr>
            <td></td>
            <td><form:hidden  path="id" /></td>
         </tr>
         <tr>
            <td>Name : </td>
            <td><form:input path="name"  /><form:errors path="name" /></td>
          </tr>
          <tr>
            <td>Description :</td>
            <td><form:input path="description" /><form:errors path="description" /></td>
          </tr>
          <tr>
            <td> </td>
            <td><input type="submit" value="Save" /></td>
          </tr>
         <tr>
            <td></td>
            <td><input type="hidden"
                         name="${_csrf.parameterName}"
                         value="${_csrf.token}" /></td>
         </tr>
     </table>
</form:form>