<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Add New Book</h1>
<form method="post" enctype="multipart/form-data">
     <table >
          <tr>
               <td>Name : </td>
               <td><input type="text" name="name"  /></td>
          </tr>
          <tr>
               <td>Description :</td>
               <td><input type="text" name="description"/></td>
          </tr>
          <tr>
               <td></td>
               <td><input type="submit" value="addBook"/></td>
          </tr>
          <tr>
               <td></td>
               <td><input type="hidden"
                          name="${_csrf.parameterName}"
                          value="${_csrf.token}" /></td>
          </tr>
     </table>


     <p>${hiddenMessage} </>
</form>