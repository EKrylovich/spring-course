package spring.course.beans;

import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;
import spring.course.conditions.MagicCondition;

@Component
@Conditional(MagicCondition.class)
public class MagicBean {

    public MagicBean() {
        System.out.println("Magic is around");
    }
}
