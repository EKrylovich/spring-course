package spring.course.beans;

import org.springframework.stereotype.Component;

@Component
public class SimpleBean {

    public void test(){
        System.out.println("Simple bean");
    }
}
