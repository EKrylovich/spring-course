package spring.course;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.beans.SimpleBean;
import spring.course.configs.Config;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);

        SimpleBean bean = applicationContext.getBean(SimpleBean.class);

        bean.test();
    }
}
