CREATE TABLE BOOK(
    id IDENTITY PRIMARY KEY,
    name VARCHAR(30),
    username VARCHAR(30),
    description VARCHAR(255));