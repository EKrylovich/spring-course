package spring.course.methodsecurity.service;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Service;
import spring.course.methodsecurity.model.Book;
import spring.course.methodsecurity.repository.BookRepository;

import javax.swing.plaf.synth.SynthEditorPaneUI;
import java.util.List;

@Service
public class BookService implements IBookService{
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Secured("ROLE_ADMIN")
    public Long addBook(Book book) {
        return bookRepository.save(book).getId();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') and #book.name.length() <= 30")
    public void updateBook(Book book) {
        if (bookRepository.existsById(book.getId()))
            bookRepository.save(book);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteBook(Long id) {
        System.out.println("We are inside delete Book");
        bookRepository.deleteById(id);
    }

    @PreAuthorize("hasAnyRole({'ROLE_USER', 'ROLE_ADMIN'})")
    @PostFilter( "hasRole('ROLE_ADMIN') || "
            + "filterObject.username == principal.username")
    public List<Book> books() {
        return bookRepository.findAll();
    }

    @PostAuthorize("returnObject.username == principal.username")
    public Book findById(Long id){
        return bookRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Secured("ROLE_USER")
    public Book findByName(String name){
        return bookRepository.findByName(name);
    }

    @Secured("ROLE_USER")
    public Book findByQueryName(String name){
        return bookRepository.findByQueryName(name);
    }

    @PreAuthorize("hasAnyRole({'ROLE_USER', 'ROLE_ADMIN'})")
    @PreFilter("hasPermission(filterObject, 'batchCreate')")
    public void batchCreateBooks(List<Book> books) {
        bookRepository.batchCreate(books);
    }
}
