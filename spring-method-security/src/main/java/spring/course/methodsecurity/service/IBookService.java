package spring.course.methodsecurity.service;

import spring.course.methodsecurity.model.Book;

import java.util.List;

public interface IBookService {
    Long addBook(Book book);

    void updateBook(Book book);

    void deleteBook(Long id);

    List<Book> books();

    Book findById(Long id);

    Book findByName(String name);

    Book findByQueryName(String name);

    void batchCreateBooks(List<Book> books);
}
