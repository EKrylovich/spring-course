package spring.course.methodsecurity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.methodsecurity.config.HibernateConfiguration;
import spring.course.methodsecurity.model.Book;
import spring.course.methodsecurity.model.User;
import spring.course.methodsecurity.service.IBookService;
import spring.course.methodsecurity.tech.security.LoginService;

import java.util.ArrayList;
import java.util.List;


public class Application {

    public static void main(String[] args) {
        justDoIt();
    }

    private static void justDoIt(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(HibernateConfiguration.class);

        IBookService bookService = applicationContext.getBean("bookService", IBookService.class);
        LoginService loginService = applicationContext.getBean("loginService", LoginService.class);

        User admin = new User("admin", "admin_pss");
        User user = new User("user", "user_pss");


        System.out.println("----------------------- All books");

        loginService.login(user);

        System.out.println(bookService.books());

        System.out.println("----------------------- Login");

        loginService.login(admin);

        System.out.println("----------------------- Adding book");
        Book book = new Book("Sherlock", "Good detective", "user");
        System.out.println(bookService.addBook(book));
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Updating book");
        book.setId(4L);
        book.setName("Sherlock2");
        bookService.updateBook(book);
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Find by id = 1");
        loginService.login(user);
        Book book1Id = bookService.findById(1L);
        System.out.println(book1Id);

        System.out.println("----------------------- Deleting books");
        loginService.login(admin);
        bookService.deleteBook(4L);
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Find by Sherlock2 name");
        System.out.println(bookService.findByName("FirstBook"));
        System.out.println("----------------------- Find by QUERY Sherlock2 name");
        System.out.println(bookService.findByQueryName("FirstBook"));


        System.out.println("----------------------- Batch create");

        loginService.login(user);
        List<Book> books = new ArrayList<>();
        books.add(new Book("Sherlock", "Good detective", "user"));
        books.add(new Book("Watson", "Good one", "user"));
        books.add(new Book("Island", "Good adv", "admin"));
        bookService.batchCreateBooks(books);
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
    }
}
