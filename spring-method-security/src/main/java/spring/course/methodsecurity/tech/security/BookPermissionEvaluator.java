package spring.course.methodsecurity.tech.security;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import spring.course.methodsecurity.model.Book;

import java.io.Serializable;

public class BookPermissionEvaluator implements PermissionEvaluator {

    private static final GrantedAuthority ADMIN_AUTHORITY =
            new SimpleGrantedAuthority("ROLE_ADMIN");

    public boolean hasPermission(Authentication authentication,
                                 Object target, Object permission) {
        if (target instanceof Book) {
            Book book = (Book) target;
            String username = book.getUsername();
            if ("batchCreate".equals(permission)) {
                return isAdmin(authentication) ||
                        username.equals(authentication.getName());
            } }
        throw new UnsupportedOperationException(
                "hasPermission not supported for object <" + target
                        + "> and permission <" + permission + ">");
    }

    public boolean hasPermission(Authentication authentication,
                                 Serializable targetId, String targetType, Object permission) {
        throw new UnsupportedOperationException();
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(ADMIN_AUTHORITY);
    }
}
