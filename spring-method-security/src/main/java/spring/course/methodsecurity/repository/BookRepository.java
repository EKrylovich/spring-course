package spring.course.methodsecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import spring.course.methodsecurity.model.Book;


@Repository
public interface BookRepository extends JpaRepository<Book, Long>,
        BookBatchRepository {

    Book findByName(String name);

    @Query("select b from Book b where b.name = :name")
    Book findByQueryName(@Param("name") String name);
}
