package spring.course.methodsecurity.repository;

import spring.course.methodsecurity.model.Book;

import java.util.List;

public interface BookBatchRepository {
    void batchCreate(List<Book> books);
}
