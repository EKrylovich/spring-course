package spring.course.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class Watches {

    private final String time;

    public Watches(@Value("#{T(System).currentTimeMillis()}") Long time) {
        this.time = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()).toString();
    }

    public String getTime() {
        return time;
    }
}
