package spring.course.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class HowAreYou {
    private String message;

//    @Autowired - not required in new version of spring
    public HowAreYou(@Value("${howAreYouMessage}") String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
