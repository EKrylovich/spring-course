package spring.course.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AnotherHowAreYou {
    private final String message;

    public AnotherHowAreYou(@Value("#{howAreYou.getMessage()}") String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
