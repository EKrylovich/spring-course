package spring.course;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.config.Config;
import spring.course.model.HowAreYou;
import spring.course.model.AnotherHowAreYou;
import spring.course.model.Message;
import spring.course.model.Watches;

public class RuntimeValues {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);


        Message helloMessage = applicationContext.getBean("helloMessage", Message.class);
        Message byeMessage = applicationContext.getBean("byeMessage", Message.class);
        HowAreYou howAreYouMessage = applicationContext.getBean("howAreYou", HowAreYou.class);
        AnotherHowAreYou anotherHowAreYou = applicationContext.getBean("anotherHowAreYou", AnotherHowAreYou.class);
        Watches watches = applicationContext.getBean("watches", Watches.class);


        System.out.println(helloMessage.getMessage());
        System.out.println(byeMessage.getMessage());
        System.out.println(howAreYouMessage.getMessage());
        System.out.println(anotherHowAreYou.getMessage());
        System.out.println(watches.getTime());
    }
}
