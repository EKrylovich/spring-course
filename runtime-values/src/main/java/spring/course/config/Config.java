package spring.course.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import spring.course.model.HowAreYou;
import spring.course.model.Message;

@Configuration
@ComponentScan("spring.course.model")
@PropertySource("classpath:application.properties")
public class Config implements EnvironmentAware{

    @Autowired
    private Environment env;


    @Bean
    public Message helloMessage() {
        return new Message(env.getProperty("hello", "Hey, man"));
    }

    @Bean
    public Message byeMessage() {
        return new Message(env.getProperty("byeMessage", "Good bye"));
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.env = environment;
    }
}
