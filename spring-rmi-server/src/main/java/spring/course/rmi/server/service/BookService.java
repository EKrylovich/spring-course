package spring.course.rmi.server.service;

import org.springframework.stereotype.Service;
import spring.course.rmi.server.model.Book;

@Service
public class BookService implements IBookService{
    @Override
    public Book getBook() {
        return new Book("BookService");
    }
}
