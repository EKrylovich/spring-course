package spring.course.rmi.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import spring.course.rmi.server.service.IBookService;

@Configuration
@ComponentScan("spring.course.rmi.server")
public class RmiConfig {
    @Bean
    public RmiServiceExporter exporter(IBookService implementation) {
        Class<IBookService> serviceInterface = IBookService.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(implementation);
        exporter.setServiceName(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }
}
