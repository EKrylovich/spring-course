package spring.course.rmi.server.service;

import spring.course.rmi.server.model.Book;

public interface IBookService {
    Book getBook();
}
