package spring.course.rmi.server;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.remoting.rmi.RmiServiceExporter;
import spring.course.rmi.server.config.RmiConfig;

public class Application {

    public static void main(String[] args) {

        new AnnotationConfigApplicationContext(RmiConfig.class);
    }
}
