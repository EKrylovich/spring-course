package spring.course.jdbc.repository;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcInsertOperations;
import org.springframework.jdbc.object.GenericSqlQuery;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import spring.course.jdbc.model.Book;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookRepository {
    String sql = "select * from BOOK";

    private JdbcOperations jdbc;
    private SimpleJdbcInsertOperations jdbcInsert;
    private GenericSqlQuery<Book> getAllBooks;
    private NamedParameterJdbcOperations namedParameterJdbc;

    public BookRepository(DataSource dataSource) {
        jdbc = new JdbcTemplate(dataSource);
        jdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("BOOK");
        getAllBooks = new GenericSqlQuery<>();
        getAllBooks.setDataSource(dataSource);
        getAllBooks.setSql(sql);
        getAllBooks.setRowMapper(bookRowMapper());
        namedParameterJdbc = new NamedParameterJdbcTemplate(dataSource);
    }

    private RowMapper<Book> bookRowMapper(){
        return (rs, rowNum) -> {
            Book book = new Book();
            book.setId(rs.getLong("id"));
            book.setName(rs.getString("name"));
            book.setDescription(rs.getString("description"));
            return book;
        };
    }

    public Long addBook(Book book) {
        final String INSERT_SQL = "insert into BOOK (name, description) values(?, ?)";
        final String name = book.getName();
        final String description = book.getDescription();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbc.update(connection -> {
                    PreparedStatement ps = connection.prepareStatement(INSERT_SQL, new String[]{"id"});
                    ps.setString(1, name);
                    ps.setString(2, description);
                    return ps;
                },
                keyHolder);
        return keyHolder.getKey().longValue();
    }

    public void updateBook(Book book) {
        final String UPDATE_SQL = "UPDATE BOOK SET name=:name, description=:description WHERE id=:id";
        final Map<String, Object> params = new HashMap<>();
        params.put("id", book.getId());
        params.put("name", book.getName());
        params.put("description", book.getDescription());
        namedParameterJdbc.update(UPDATE_SQL, params);
    }

    public void deleteBook(Long id) {
        jdbc.update("delete from BOOK where id = ?", id);
    }

    public List<Book> books() {
        return getAllBooks.execute();
    }

    public int countBooks() {
        return jdbc.queryForObject("select count(*) from BOOK", Integer.class);
    }

    public void batchCreateBooks(List<Book> books) {
        Map[] objects = books.stream().map(book -> {
            Map<String, Object> object = new HashMap<>();
            object.put("name", book.getName());
            object.put("description", book.getDescription());
            return object;
        }).toArray(Map[]::new);
        jdbcInsert.executeBatch(objects);
    }
}
