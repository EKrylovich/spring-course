package spring.course.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.jdbc.config.JdbcConfiguration;
import spring.course.jdbc.model.Book;
import spring.course.jdbc.repository.BookRepository;

import java.util.Arrays;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(JdbcConfiguration.class);

        BookRepository bookRepository = applicationContext.getBean("bookRepository", BookRepository.class);


        System.out.println("----------------------- All books");
        System.out.println(bookRepository.books());


        System.out.println("----------------------- Adding book");
        Book book = new Book("Sherlock", "Good detective");
        System.out.println(bookRepository.addBook(book));
        System.out.println("----------------------- All books");
        System.out.println(bookRepository.books());
        System.out.println("----------------------- Updating book");
        book.setId(4L);
        book.setName("Sherlock2");
        bookRepository.updateBook(book);
        System.out.println("----------------------- All books");
        System.out.println(bookRepository.books());
        System.out.println("----------------------- AddingBatch books");
        Book book1 = new Book("Sherlock", "Good detective");
        Book book2 = new Book("Watson", "Good one");
        Book book3 = new Book("Island", "Good adv");
        bookRepository.batchCreateBooks(Arrays.asList(book1, book2, book3));
        System.out.println("----------------------- All books");
        System.out.println(bookRepository.books());

        System.out.println("----------------------- Deleting books");
        bookRepository.deleteBook(4L);
        System.out.println("----------------------- All books");
        System.out.println(bookRepository.books());

    }
}
