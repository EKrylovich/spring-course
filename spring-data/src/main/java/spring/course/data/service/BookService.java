package spring.course.data.service;

import org.springframework.stereotype.Service;
import spring.course.data.model.Book;
import spring.course.data.repository.BookRepository;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Long addBook(Book book) {
        return bookRepository.save(book).getId();
    }

    public void updateBook(Book book) {
        if (bookRepository.existsById(book.getId()))
            bookRepository.save(book);
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public List<Book> books() {
        return bookRepository.findAll();
    }

    public Book findById(Long id){
        return bookRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }
    public Book findByName(String name){
        return bookRepository.findByName(name);
    }

    public Book findByQueryName(String name){
        return bookRepository.findByQueryName(name);
    }

    public void batchCreateBooks(List<Book> books) {
        bookRepository.batchCreate(books);
    }
}
