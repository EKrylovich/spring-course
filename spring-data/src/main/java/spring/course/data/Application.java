package spring.course.data;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.data.config.HibernateConfiguration;
import spring.course.data.model.Book;
import spring.course.data.service.BookService;

import java.util.Arrays;

public class Application {

    public static void main(String[] args) {
        justDoIt();
    }

    private static void justDoIt(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(HibernateConfiguration.class);

        BookService bookService = applicationContext.getBean("bookService", BookService.class);



        System.out.println("----------------------- All books");
        System.out.println(bookService.books());


        System.out.println("----------------------- Adding book");
        Book book = new Book("Sherlock", "Good detective");
        System.out.println(bookService.addBook(book));
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Updating book");
        book.setId(4L);
        book.setName("Sherlock2");
        bookService.updateBook(book);
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Find by id = 1");
        Book book1Id = bookService.findById(1L);
        System.out.println(book1Id);

        System.out.println("----------------------- Deleting books");
        bookService.deleteBook(4L);
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
        System.out.println("----------------------- Find by Sherlock2 name");
        System.out.println(bookService.findByName("FirstBook"));
        System.out.println("----------------------- Find by QUERY Sherlock2 name");
        System.out.println(bookService.findByQueryName("FirstBook"));

        Book book1 = new Book("Sherlock", "Good detective");
        Book book2 = new Book("Watson", "Good one");
        Book book3 = new Book("Island", "Good adv");
        bookService.batchCreateBooks(Arrays.asList(book1, book2, book3));
        System.out.println("----------------------- All books");
        System.out.println(bookService.books());
    }
}
