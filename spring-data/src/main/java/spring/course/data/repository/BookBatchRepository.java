package spring.course.data.repository;

import spring.course.data.model.Book;

import java.util.List;

public interface BookBatchRepository {
    void batchCreate(List<Book> books);
}
