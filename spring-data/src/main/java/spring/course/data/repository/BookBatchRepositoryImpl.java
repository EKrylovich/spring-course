package spring.course.data.repository;

import org.springframework.transaction.annotation.Transactional;
import spring.course.data.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class BookBatchRepositoryImpl implements BookBatchRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void batchCreate(List<Book> books) {
        books.forEach(em::persist);
    }
}
