package instruments;

public class UniversalInstrument  implements Instruments {
    private final String instrumentName;


    public UniversalInstrument(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    @Override
    public void repair() {
        System.out.println("Fixing issues with " + instrumentName);
    }
}
