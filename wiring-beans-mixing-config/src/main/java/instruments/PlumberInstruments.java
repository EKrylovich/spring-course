package instruments;

public class PlumberInstruments implements Instruments {
    @Override
    public void repair() {
        System.out.println("Fixing plumbing issues");
    }
}
