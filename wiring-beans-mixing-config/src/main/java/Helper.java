import config.Config;
import helper.HouseHelper;
import helper.OfficeHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Helper {

    public static void main(String[] args) {
        ApplicationContext annotationContext = new AnnotationConfigApplicationContext(Config.class);

        HouseHelper plumber = annotationContext.getBean("plumber", HouseHelper.class);
        HouseHelper builder = annotationContext.getBean("builder", HouseHelper.class);
        HouseHelper hammerHelper = annotationContext.getBean("hammerHelper", HouseHelper.class);
        HouseHelper emptyHelper = annotationContext.getBean("emptyHelper", HouseHelper.class);
        HouseHelper amazingHelper = annotationContext.getBean("amazingHelper", HouseHelper.class);
        HouseHelper kitHouseHelper = annotationContext.getBean("kitHouseHelper", HouseHelper.class);
        HouseHelper kitHelper = annotationContext.getBean("kitHelper", HouseHelper.class);
        OfficeHelper officePlumber = annotationContext.getBean("officePlumber", OfficeHelper.class);
        OfficeHelper officeKit = annotationContext.getBean("officeKit", OfficeHelper.class);

        builder.fixBuilding();
        plumber.fixBuilding();
        hammerHelper.fixBuilding();
        emptyHelper.fixBuilding();
        amazingHelper.fixBuilding();
        kitHouseHelper.fixBuilding();
        kitHelper.fixBuilding();
        officePlumber.fixBuilding();
        officeKit.fixBuilding();
    }
}
