package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({HouseConfig.class, OfficeConfig.class})
@ImportResource("spring-helper-context.xml")
public class Config {
}
