package config;

import config.instruments.InstrumentKitConfig;
import helper.HouseHelper;
import instruments.BuilderInstruments;
import instruments.InstrumentsKit;
import instruments.PlumberInstruments;
import instruments.UniversalInstrument;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

import java.util.List;

@Configuration
@Import(InstrumentKitConfig.class)
@ImportResource({"classpath:spring-instruments-context.xml"})
public class HouseConfig {
    @Bean
    public HouseHelper builder(BuilderInstruments builderInstruments){
        return new HouseHelper(builderInstruments);
    }

    @Bean
    public HouseHelper plumber(PlumberInstruments plumberInstruments){
        return new HouseHelper(plumberInstruments);
    }

    @Bean
    public HouseHelper hammerHelper(UniversalInstrument hammerInstruments){
        return new HouseHelper(hammerInstruments);
    }

    @Bean
    public HouseHelper emptyHelper(UniversalInstrument emptyInstruments){
        return new HouseHelper(emptyInstruments);
    }

    @Bean
    public HouseHelper amazingHelper(@Qualifier("amazingInstruments") UniversalInstrument instrument){
        return new HouseHelper(instrument);
    }

    @Bean
    public HouseHelper kitHouseHelper(@Qualifier("smallKit") InstrumentsKit instrument){
        return new HouseHelper(instrument);
    }


}
