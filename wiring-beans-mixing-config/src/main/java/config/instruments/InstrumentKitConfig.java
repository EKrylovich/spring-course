package config.instruments;

import instruments.InstrumentsKit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class InstrumentKitConfig {

    @Bean
    public InstrumentsKit instrumentsKit(){
        return new InstrumentsKit(Arrays.asList("Hammer", "Screwdriver", "Sledgehammer"));
    }

    @Bean
    public InstrumentsKit smallKit(){
        return new InstrumentsKit(Arrays.asList("Hammer", "Screwdriver"));
    }
}
