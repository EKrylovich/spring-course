package config;

import helper.OfficeHelper;
import instruments.BuilderInstruments;
import instruments.InstrumentsKit;
import instruments.PlumberInstruments;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource("classpath:spring-instruments-context.xml")
public class OfficeConfig {

    @Bean
    public OfficeHelper officePlumber(PlumberInstruments plumberInstruments){
        OfficeHelper officeHelper = new OfficeHelper();
        officeHelper.setInstruments(plumberInstruments);
        return officeHelper;
    }

    @Bean
    public OfficeHelper officeKit(@Qualifier("smallKit") InstrumentsKit instrumentsKit) {
        OfficeHelper officeHelper = new OfficeHelper();
        officeHelper.setInstruments(instrumentsKit);
        return officeHelper;
    }
}
