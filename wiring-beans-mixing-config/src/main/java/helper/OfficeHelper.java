package helper;

import instruments.Instruments;

public class OfficeHelper {
    private Instruments instruments;

    public void fixBuilding(){
        System.out.println("Start fixing office issues");
        instruments.repair();
        System.out.println("Finished fixing office issues");
    }

    public void setInstruments(Instruments instruments) {
        this.instruments = instruments;
    }
}
