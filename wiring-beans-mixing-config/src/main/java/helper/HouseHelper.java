package helper;

import instruments.Instruments;

public class HouseHelper {
    private final Instruments instruments;

    public HouseHelper(Instruments instruments){
        this.instruments = instruments;
    }

    public void fixBuilding(){
        System.out.println("Start fixing house issues");
        instruments.repair();
        System.out.println("Finished fixing house issues");
    }
}
