package spring.course.cache.config;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import spring.course.cache.tech.CustomCacheManager;
import spring.course.cache.tech.SimpleMapCache;

@Configuration
@EnableCaching
@ComponentScan("spring.course.cache")
public class CacheConfig {
    @Bean
    public CacheManager cacheManager() {
        CustomCacheManager customCacheManager = new CustomCacheManager();
        customCacheManager.addCache(new SimpleMapCache("book"));
        return customCacheManager;
    }
}
