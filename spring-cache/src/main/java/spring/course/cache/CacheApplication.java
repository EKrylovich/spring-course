package spring.course.cache;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.cache.config.CacheConfig;
import spring.course.cache.model.Book;
import spring.course.cache.service.BookService;

import java.util.Arrays;

public class CacheApplication {
    public static void main(String[] args) {
        justDoIt();
    }

    private static void justDoIt(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(CacheConfig.class);

        BookService bookService = applicationContext.getBean("bookService", BookService.class);

        System.out.println("----------------------- Adding book");
        Book book = new Book("Sherlock", "Good detective");
        System.out.println(bookService.addBook(book));

        System.out.println("----------------------- Find by id = 2");
        System.out.println(bookService.findById(2L));
        System.out.println("----------------------- Find by id without cache = 2");
        System.out.println(bookService.findByIdWithoutCache(2L));
        System.out.println();
        System.out.println();
        System.out.println();


        System.out.println("----------------------- Deleting books without evict");
        bookService.deleteWithoutEvict(2L);
        System.out.println("----------------------- Find by id = 1");
        System.out.println(bookService.findById(2L));
        System.out.println("----------------------- Find by id without cache = 1");
        System.out.println(bookService.findByIdWithoutCache(2L));
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("----------------------- Deleting books with evict");
        bookService.deleteBook(2L);
        System.out.println("----------------------- Find by id = 2");
        System.out.println(bookService.findById(2L));
        System.out.println("----------------------- Find by id without cache = 2");
        System.out.println(bookService.findByIdWithoutCache(2L));
        System.out.println();
        System.out.println();
        System.out.println();
    }
}
