package spring.course.cache.tech;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomCacheManager implements CacheManager {
    private final Map<String, Cache> cacheMap = new HashMap<>();

    @Override
    public Cache getCache(String name) {
        return cacheMap.get(name);
    }

    @Override
    public Collection<String> getCacheNames() {
        return cacheMap.keySet();
    }

    public void addCache(Cache cache){
        cacheMap.put(cache.getName(), cache);
    }
}
