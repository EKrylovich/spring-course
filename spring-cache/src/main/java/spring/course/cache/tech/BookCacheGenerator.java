package spring.course.cache.tech;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;
import spring.course.cache.model.Book;

import java.lang.reflect.Method;

@Component
public class BookCacheGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        return ((Book)params[0]).getId();
    }
}
