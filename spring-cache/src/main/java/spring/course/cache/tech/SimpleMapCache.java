package spring.course.cache.tech;

import org.springframework.cache.Cache;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class SimpleMapCache implements Cache {

    private final String cacheName;
    private Map<Object, Object> cacheStore = new HashMap<>();

    public SimpleMapCache(String cacheName) {
        this.cacheName = cacheName;
    }


    @Override
    public String getName() {
        return cacheName;
    }

    @Override
    public Object getNativeCache() {
        return cacheStore;
    }

    @Override
    public ValueWrapper get(Object key) {
        return () -> cacheStore.get(key);
    }

    @Override
    public <T> T get(Object key, Class<T> type) {
        return type.cast(cacheStore.get(key));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(Object key, Callable<T> valueLoader) {
        return (T) cacheStore.computeIfAbsent(key, objKey -> {
            try {
                return valueLoader.call();
            } catch (Throwable ex) {
                throw new ValueRetrievalException(key, valueLoader, ex);
            }
        });
    }

    @Override
    public void put(Object key, Object value) {
        cacheStore.put(key, value);
    }

    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        return () -> cacheStore.putIfAbsent(key, value);
    }

    @Override
    public void evict(Object key) {
        cacheStore.remove(key);
    }

    @Override
    public void clear() {
        cacheStore.clear();
    }
}
