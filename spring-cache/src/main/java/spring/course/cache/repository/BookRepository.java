package spring.course.cache.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import spring.course.cache.model.Book;

import java.util.*;

@Repository
public class BookRepository {

    private Map<Long, Book> storage = new HashMap<>();
    private static Long id = 0L;

    {
        storage.put(id, new Book(id,"FirstBook_Stored", "FirstDescription"));
        id++;
        storage.put(id, new Book(id,"SecondBook_Stored", "SecondDescription"));
        id++;
    }


    @CachePut(value = "book", key = "#book.getId()")
    public Book save(Book book) {
        Book value = new Book(id, book.getName() + "_Stored", book.getDescription());
        storage.put(id, value);
        book.setId(id);
        id++;
        return book;
    }

    @CachePut(value = "book", keyGenerator = "bookCacheGenerator")
    public Book update(Book book) {
        Book value = new Book(id, book.getName() + "_Stored", book.getDescription());
        storage.put(id, value);
        book.setId(id);
        id++;
        return book;
    }

    public boolean existsById(Long id) {
        return storage.containsKey(id);
    }

    @CacheEvict(value = "book")
    public void deleteById(Long id) {
        storage.remove(id);
    }

    public void deleteByIdWithoutEvict(Long id) {
        storage.remove(id);
    }

    @Cacheable(value = "book")
    public Book findById(Long id) {
        return storage.get(id);
    }

    public Book findByIdWithout(Long id) {
        return storage.get(id);
    }
}
