package spring.course.cache.service;

import org.springframework.stereotype.Service;
import spring.course.cache.model.Book;
import spring.course.cache.repository.BookRepository;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Long addBook(Book book) {
        return bookRepository.save(book).getId();
    }

    public Book updateBook(Book book) {
        if (bookRepository.existsById(book.getId()))
            return bookRepository.update(book);
        return null;
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public Book findById(Long id){
        return bookRepository.findById(id);
    }

    public Book findByIdWithoutCache(Long id){
        return bookRepository.findByIdWithout(id);
    }

    public void deleteWithoutEvict(Long id){
        bookRepository.deleteByIdWithoutEvict(id);
    }
}
