package test;

import def.Config;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.Writer;

import static org.junit.Assert.assertEquals;
import static test.TestConfig.TEST_WRITER;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Config.class})
@ActiveProfiles("test-env")
public class TestConfigTest {

    @Autowired
    private Writer writer;

    @Test
    public void testTest(){
        assertEquals(TEST_WRITER, writer.getMessage());
    }
}
