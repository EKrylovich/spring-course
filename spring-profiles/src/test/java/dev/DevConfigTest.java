package dev;

import def.Config;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.Writer;

import static dev.DevConfig.DEV_WRITER;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Config.class})
@ActiveProfiles("dev-env")
public class DevConfigTest {

    @Autowired
    private Writer writer;

    @Test
    public void devTest(){
        assertEquals(DEV_WRITER, writer.getMessage());
    }
}
