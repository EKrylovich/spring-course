package service;

public class Writer {
    private final String message;


    public Writer(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
