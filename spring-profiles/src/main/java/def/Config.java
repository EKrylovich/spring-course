package def;

import dev.DevConfig;
import org.springframework.context.annotation.*;
import service.Writer;
import test.TestConfig;

@Configuration
@Import({TestConfig.class, DevConfig.class})
public class Config {

    public static final String DEFAULT_WRITER = "Default writer";

    @Bean
    @Profile("default")
    public Writer writer(){
        return new Writer(DEFAULT_WRITER);
    }
}
