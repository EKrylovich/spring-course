package dev;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import service.Writer;

@Configuration
@Profile("dev-env")
public class DevConfig {

    public static final String DEV_WRITER = "Dev writer";

    @Bean
    public Writer writer(){
        return new Writer(DEV_WRITER);
    }
}
