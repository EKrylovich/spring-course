package test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import service.Writer;

@Configuration
@Profile("test-env")
public class TestConfig {

    public static final String TEST_WRITER = "Test writer";

    @Bean
    public Writer writer(){
        return new Writer(TEST_WRITER);
    }
}
