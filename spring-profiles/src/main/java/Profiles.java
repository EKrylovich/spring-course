import def.Config;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.Writer;

public class Profiles {

    public static void main(String[] args) {
        ApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);

        Writer writer = appContext.getBean(Writer.class);

        System.out.println(writer.getMessage());
    }
}
