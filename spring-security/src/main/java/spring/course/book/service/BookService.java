package spring.course.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.course.book.model.Book;
import spring.course.book.repository.BookRepository;
import spring.course.tech.RequestBookName;

import javax.inject.Provider;
import java.util.Set;

@Service
public class BookService {
    private final BookRepository bookRepository;
    private final Provider<RequestBookName> bookNameProvider;

    @Autowired
    public BookService(BookRepository bookRepository, Provider<RequestBookName> bookNameProvider) {
        this.bookRepository = bookRepository;
        this.bookNameProvider = bookNameProvider;
    }

    public Book addBook(Book book){
        return bookRepository.save(book);
    }

    public Set<Book> books(){
        return bookRepository.books();
    }

    public Book bookById(Long id){
        return bookRepository.book(id);
    }

    public void deleteBook(Long id){
        String name = bookNameProvider.get().getName();
        bookRepository.deleteBook(id);
    }

    public void editBook(Book book){
        bookRepository.editBook(book);
    }
}
