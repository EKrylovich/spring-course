package instruments;

import config.InstrumentsConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = InstrumentsConfig.class)
public class BuilderInstrumentsBeanTest {
    @Autowired
    BuilderInstruments builderInstruments;

    @Test
    public void testBuilderInstrumentsNotNull(){
        assertNotNull(builderInstruments);
    }

}
