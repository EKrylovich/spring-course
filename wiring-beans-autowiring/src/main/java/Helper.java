import config.Config;
import helper.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Helper {

    public static void main(String[] args) {
        ApplicationContext annotationContext = new AnnotationConfigApplicationContext(Config.class);

        HouseHelper plumber = annotationContext.getBean("namedHouseHelper", HouseHelper.class);
        OfficeHelper builder = annotationContext.getBean("officeHelper", OfficeHelper.class);
        UniversalHelper universalHelper = annotationContext.getBean("universalHelper", UniversalHelper.class);
        CottageHelper cottageHelper = annotationContext.getBean("cottageHelper", CottageHelper.class);
        GardenHelper gardenHelper = annotationContext.getBean("gardenHelper", GardenHelper.class);

        builder.fixBuilding();
        plumber.fixBuilding();
        universalHelper.fixing();
        cottageHelper.fixing();
        gardenHelper.fixing();
    }
}
