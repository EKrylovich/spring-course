package config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({HelperConfig.class, InstrumentsConfig.class})
public class Config {
}
