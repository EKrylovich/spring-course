package helper;

import instruments.Instruments;
import instruments.PlumberInstruments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qualifier.Garden;
import qualifier.TreeInstruments;

@Component
public class GardenHelper {

    @Autowired
    @Garden
    @TreeInstruments
    private Instruments instruments;

    public void fixing(){
        instruments.repair();
    }
}
