package helper;

import instruments.BuilderInstruments;
import instruments.Instruments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class OfficeHelper {
    private Instruments instruments;

    public void fixBuilding(){
        instruments.repair();
    }

    @Autowired
    public void setInstruments(@Qualifier("mainOfficeInstruments") Instruments instruments) {
        this.instruments = instruments;
    }
}
