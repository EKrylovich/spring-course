package helper;

import instruments.Instruments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UniversalHelper {
    private final Instruments instruments;

    @Autowired
    public UniversalHelper(Instruments instruments) {
        this.instruments = instruments;
    }

    public void fixing(){
        instruments.repair();
    }
}
