package helper;

import instruments.Instruments;
import instruments.PlumberInstruments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("namedHouseHelper")
public class HouseHelper {
    private final PlumberInstruments instruments;

    @Autowired
    public HouseHelper(PlumberInstruments instruments){
        this.instruments = instruments;
    }

    public void fixBuilding(){
        instruments.repair();
    }
}
