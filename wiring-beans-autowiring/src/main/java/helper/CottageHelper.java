package helper;

import instruments.GardenInstruments;
import instruments.Instruments;
import instruments.PlumberInstruments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CottageHelper {
    private final Instruments instruments;

    @Autowired
    public CottageHelper(@Qualifier("gardenInstruments") Instruments instruments){
        this.instruments = instruments;
    }

    public void fixing(){
        instruments.repair();
    }
}
