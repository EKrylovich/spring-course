package instruments;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class UniversalInstruments implements Instruments {
    @Override
    public void repair() {
        System.out.println("Fixing any issues");
    }
}
