package instruments;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import qualifier.Forrest;
import qualifier.Garden;
import qualifier.TreeInstruments;

@Component
@TreeInstruments
@Forrest
public class ForestInstruments implements Instruments {

    @Override
    public void repair() {
        System.out.println("Fixing forest issues");
    }
}
