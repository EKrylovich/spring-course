package instruments;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import qualifier.Garden;
import qualifier.TreeInstruments;

@Component
@TreeInstruments
@Garden
public class GardenInstruments implements Instruments {

    @Override
    public void repair() {
        System.out.println("Fixing garden issues");
    }
}
