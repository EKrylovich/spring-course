package instruments;

import org.springframework.stereotype.Component;

@Component
public class PlumberInstruments implements Instruments {
    @Override
    public void repair() {
        System.out.println("Fixing plumbing issues");
    }
}
