package instruments;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("mainOfficeInstruments")
public class BuilderInstruments implements Instruments {
    @Override
    public void repair() {
        System.out.println("Fixing building issues");
    }
}
