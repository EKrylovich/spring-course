package spring.course.rmi.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import org.springframework.remoting.rmi.RmiServiceExporter;
import spring.course.rmi.client.service.IBookService;

@Configuration
@ComponentScan("spring.course.rmi.client")
public class RmiConfig {
    @Bean
    RmiProxyFactoryBean service() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/IBookService");
        rmiProxyFactory.setServiceInterface(IBookService.class);
        return rmiProxyFactory;
    }
}