package spring.course.rmi.client.service;

import spring.course.rmi.server.model.Book;

public interface IBookService {
    Book getBook();
}
