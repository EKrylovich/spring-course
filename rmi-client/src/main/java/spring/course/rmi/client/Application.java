package spring.course.rmi.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.rmi.client.config.RmiConfig;
import spring.course.rmi.client.service.IBookService;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(RmiConfig.class);

        IBookService bookService = applicationContext.getBean(IBookService.class);

        System.out.println(bookService.getBook().getTitle());
    }
}
