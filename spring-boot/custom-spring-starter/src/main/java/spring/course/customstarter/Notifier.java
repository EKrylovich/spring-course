package spring.course.customstarter;

public class Notifier {

    private final String message;

    public Notifier(String message) {
        this.message = message;
    }

    public void notification(){
        System.out.println(message);
    }
}
