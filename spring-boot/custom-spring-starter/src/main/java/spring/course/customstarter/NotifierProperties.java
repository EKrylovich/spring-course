package spring.course.customstarter;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("custom.notifier")
public class NotifierProperties {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
