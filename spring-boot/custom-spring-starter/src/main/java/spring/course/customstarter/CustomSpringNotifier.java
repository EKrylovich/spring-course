package spring.course.customstarter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnMissingBean(Notifier.class)
@EnableConfigurationProperties(NotifierProperties.class)
public class CustomSpringNotifier {

    @Bean
    @ConditionalOnProperty("custom.notifier.message")
    public Notifier notification(NotifierProperties notifierProperties){
        System.out.println("Notifier creation");
        return new Notifier(notifierProperties.getMessage());
    }

}
