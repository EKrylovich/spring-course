package spring.course.boot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import spring.course.boot.model.Book;

@RestController
public class BookController {

    @GetMapping("/book")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody Book books(){
        return new Book(1L, "Sherlock", "Good detective");
    }

}
