package spring.course.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import spring.course.customstarter.Notifier;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext appContext = SpringApplication.run(Application.class);

        Notifier notifier = appContext.getBean("notification", Notifier.class);
        notifier.notification();
    }

    @Bean
    public Notifier notification(){
        return new Notifier("Application bean notifier");
    }
}
