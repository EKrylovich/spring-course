package spring.course.book.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.servlet.view.InternalResourceView;
import spring.course.book.application.model.Book;
import spring.course.book.application.service.BookService;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class BookControllerTest {

    @Mock
    private BookService bookService;
    @InjectMocks
    private BookController bookController;

    private MockMvc mockMvc;

    @Test
    public void getBookList() throws Exception {
//  Given

        mockMvc = standaloneSetup(bookController)
                .build();
        String bookName = "BookName";
        String bookDescription = "BookDescription";
        Set<Book> books = Collections.singleton(new Book(1L, bookName, bookDescription));
        when(bookService.books()).thenReturn(books);
//  WHen
        final ResultActions result = mockMvc.perform(get("/viewBooks"))
//  Then
                .andExpect(view().name("viewBooks"))
                .andExpect(model().attributeExists("books"))
                .andExpect(model().attribute("books", hasItems(books.toArray())));
    }


    @Test
    public void addBook() throws Exception {
//  Given
        mockMvc = standaloneSetup(bookController)
                .build();
        String bookName = "BookName";
        String bookDescription = "BookDescription";
        Book unSavedBook = new Book(bookName, bookDescription);
        Book savedBook = new Book(1L, bookName, bookDescription);
        when(bookService.addBook(unSavedBook)).thenReturn(savedBook);
//  WHen
        final ResultActions result = mockMvc.perform(post("/addBook")
                .param("name", bookName)
                .param("description", bookDescription))
//  Then
                .andExpect(redirectedUrl("/viewBooks"));

        verify(bookService, times(1)).addBook(unSavedBook);
        verifyNoMoreInteractions(bookService);
    }
}
