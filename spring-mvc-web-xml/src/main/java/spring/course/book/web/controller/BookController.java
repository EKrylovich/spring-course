package spring.course.book.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import spring.course.book.application.model.Book;
import spring.course.book.application.service.BookService;

import java.util.Collections;

@Controller
public class BookController {
    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "viewBooks", method = RequestMethod.GET)
    public String books(Model model) {
        model.addAttribute("books", bookService.books());
        return "viewBooks";
    }

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public String books(Model model, @RequestParam Long bookId) {
        model.addAttribute("books", Collections.singleton(bookService.bookById(bookId)));
        return "viewBooks";
    }

    @RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable Long bookId) {
        bookService.deleteBook(bookId);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public String addBook() {
        return "addBook";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(Book book) {
        Book savedBook = bookService.addBook(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/editBook/{bookId}", method = RequestMethod.GET)
    public ModelAndView editBook(ModelAndView modelAndView, @PathVariable Long bookId) {
        Book book = bookService.bookById(bookId);
        modelAndView.addObject("command", book);
        modelAndView.setViewName("editBook");
        return modelAndView;
    }

    @RequestMapping(value = "/editBook", method = RequestMethod.POST)
    public String editBook(Book book) {
        bookService.editBook(book);
        return "redirect:/viewBooks";
    }
}
