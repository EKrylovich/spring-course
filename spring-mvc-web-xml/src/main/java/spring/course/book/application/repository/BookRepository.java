package spring.course.book.application.repository;

import org.springframework.stereotype.Repository;
import spring.course.book.application.model.Book;

import javax.annotation.PostConstruct;
import java.util.*;


@Repository
public class BookRepository {

    private static Long idCounter = 0L;

    private final Map<Long, Book> storage = new HashMap<>();

    public Book save(Book book){
        book.setId(idCounter);
        storage.put(idCounter, book);
        idCounter++;
        return book;
    }

    public Set<Book> books(){
        return new HashSet<>(storage.values());
    }

    public Book book(Long id){
        return storage.get(id);
    }


    public void deleteBook(Long id){
        storage.remove(id);
    }

    @PostConstruct
    public void init(){
        storage.put(idCounter, new Book(idCounter,"Mysterious island", "Good adventure"));
        idCounter++;
        storage.put(idCounter, new Book(idCounter,"Sherlock", "Good detective"));
        idCounter++;

    }

    public void editBook(Book book) {
        storage.put(book.getId(), book);
    }
}

