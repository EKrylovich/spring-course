package spring.course.book.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.course.book.application.model.Book;
import spring.course.book.application.repository.BookRepository;

import java.util.Set;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book addBook(Book book){
        return bookRepository.save(book);
    }

    public Set<Book> books(){
        return bookRepository.books();
    }

    public Book bookById(Long id){
        return bookRepository.book(id);
    }

    public void deleteBook(Long id){
        bookRepository.deleteBook(id);
    }

    public void editBook(Book book){
        bookRepository.editBook(book);
    }
}
