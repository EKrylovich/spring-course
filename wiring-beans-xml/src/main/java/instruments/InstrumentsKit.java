package instruments;

import java.util.List;

public class InstrumentsKit implements Intruments{
    private final List<String> instruments;


    public InstrumentsKit(List<String> instruments) {
        this.instruments = instruments;
    }

    @Override
    public void repair() {
        System.out.println("Fixing issues with " + instruments);
    }
}
