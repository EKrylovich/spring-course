import helper.HouseHelper;
import helper.OfficeHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Helper {


    public static void main(String[] args) {
        ApplicationContext annotationContext = new ClassPathXmlApplicationContext("spring-context.xml");

        HouseHelper plumber = annotationContext.getBean("plumber", HouseHelper.class);
        HouseHelper builder = annotationContext.getBean("builder", HouseHelper.class);
        HouseHelper hammerHelper = annotationContext.getBean("hammerHelper", HouseHelper.class);
        HouseHelper emptyHelper = annotationContext.getBean("emptyHelper", HouseHelper.class);
        HouseHelper kitHelper = annotationContext.getBean("kitHelper", HouseHelper.class);
        OfficeHelper officePlumber = annotationContext.getBean("officePlumber", OfficeHelper.class);
        OfficeHelper officeBuilder = annotationContext.getBean("officeBuilder", OfficeHelper.class);
        OfficeHelper officeKit = annotationContext.getBean("officeKit", OfficeHelper.class);

        builder.fixBuilding();
        plumber.fixBuilding();
        hammerHelper.fixBuilding();
        emptyHelper.fixBuilding();
        kitHelper.fixBuilding();
        officePlumber.fixBuilding();
        officeBuilder.fixBuilding();
        officeKit.fixBuilding();
    }
}
