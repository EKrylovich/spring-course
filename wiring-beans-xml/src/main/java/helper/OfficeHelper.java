package helper;

import instruments.Intruments;

public class OfficeHelper {
    private Intruments instruments;

    public void fixBuilding(){
        System.out.println("Start fixing office issues");
        instruments.repair();
        System.out.println("Finished fixing office issues");
    }

    public void setInstruments(Intruments instruments) {
        this.instruments = instruments;
    }
}
