<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="en">
<head>
<title>Book</title>
</head>
<table border="2" width="70%" cellpadding="2">
    <tr><th>Id</th><th>Name</th><th>Description</th>

        <tr>
           <td>${book.id}</td>
           <td>${book.name}</td>
           <td>${book.description}</td>
        </tr>
</table>
<br/>
