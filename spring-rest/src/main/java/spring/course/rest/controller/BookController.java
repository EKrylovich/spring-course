package spring.course.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import spring.course.rest.model.Book;

@Controller
public class BookController {

    @GetMapping("/book")
    public Book books(){
        return new Book(1L, "Sherlock", "Good detective");
    }
}
