package spring.course.rest.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import spring.course.rest.model.Book;

import java.nio.charset.Charset;
import java.util.Base64;

@RestController
public class BookController {

    @GetMapping("/book")
    @ResponseStatus(HttpStatus.CREATED)
    public Book books(){
        return new Book(1L, "Sherlock", "Good detective");
    }


    @PostMapping(value = "/book", consumes="application/json")
    public void books(@RequestBody String book){
        System.out.println(book);
    }

    @GetMapping("/exception")
    public @ResponseBody void exception(){
        throw new RuntimeException("Runtime error!");
    }

    @GetMapping("/bookFromOtherService")
    public @ResponseBody Book bookFromOtherService(){
        RestTemplate rest = new RestTemplate();

        return rest.exchange
                ("url",
                        HttpMethod.POST,
                        new HttpEntity<>(createHeaders("username", "password")),
                        Book.class)
                .getBody();
    }

    HttpHeaders createHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }




}
