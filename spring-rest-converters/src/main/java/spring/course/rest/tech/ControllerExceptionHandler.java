package spring.course.rest.tech;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(Exception exp){
        return buildErrorInfo(exp, exp.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    private ResponseEntity<ApiError> buildErrorInfo(Exception exp, String message, HttpStatus status){
        ApiError apiError = new ApiError(status, LocalDateTime.now(), message, ExceptionUtils.getStackTrace(exp));
        return new ResponseEntity<>(apiError, null, status);
    }
}
