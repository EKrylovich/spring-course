package spring.course.rest.tech;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Created by ekrylovich
 * on 10/4/18
 */
public class ApiError {

    public final HttpStatus status;
    public final LocalDateTime dateTime;
    public final String message;
    public final String debugMessage;

    public ApiError(HttpStatus status, LocalDateTime dateTime, String message, String debugMessage) {
        this.status = status;
        this.dateTime = dateTime;
        this.message = message;
        this.debugMessage = debugMessage;
    }
}
