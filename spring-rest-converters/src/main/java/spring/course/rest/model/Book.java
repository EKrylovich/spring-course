package spring.course.rest.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "book")
public class Book implements Serializable {
    private Long id;
    private String name;
    private String description;

    public Book() {
        System.out.println("default constructor");
    }

    public Book(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @JsonCreator
    public Book(@JsonProperty("id") Long id,
                @JsonProperty("name") String name,
                @JsonProperty("description") String description) {
        System.out.println("pararmetrized constructor");
        this.id = id;
        this.name = name;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name +
                '}';
    }
}
