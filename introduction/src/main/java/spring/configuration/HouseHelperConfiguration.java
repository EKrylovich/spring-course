package spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import spring.instruments.PlumberInstruments;
import spring.helper.HouseHelper;
import spring.instruments.BuilderInstruments;
import spring.instruments.support.InstrumentCleaner;
import spring.instruments.Intruments;

@Configuration
@EnableAspectJAutoProxy
public class HouseHelperConfiguration {
    @Bean
    public HouseHelper builder(){
        return new HouseHelper(builderInstruments());
    }

    @Bean
    public HouseHelper plumber(){
        return new HouseHelper(plumberInstruments());
    }

    @Bean
    public Intruments builderInstruments(){
        return new BuilderInstruments();
    }

    @Bean
    public Intruments plumberInstruments(){
        return new PlumberInstruments();
    }

    @Bean
    public InstrumentCleaner cashier(){
        return new InstrumentCleaner();
    }
}
