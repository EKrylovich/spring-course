package spring.instruments;

import spring.instruments.Intruments;

public class PlumberInstruments implements Intruments {
    @Override
    public void repair() {
        System.out.println("Fixing plumbing issues");
    }
}
