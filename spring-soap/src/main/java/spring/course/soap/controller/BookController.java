package spring.course.soap.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.course.soap.model.Book;
import spring.course.soap.service.BookService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService(serviceName="BookWebService")
public class BookController {
    @Autowired
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }


    @WebMethod
    public Long createBook(Book book){
        return bookService.addBook(book);
    }

    @WebMethod
    public Book updateBook(Book book){
        return bookService.updateBook(book);
    }

    @WebMethod
    public void deleteBook(Long id){
        bookService.deleteBook(id);
    }

    @WebMethod
    public List<Book> books(){
        return bookService.books();
    }
}
