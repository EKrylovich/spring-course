package spring.course.soap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;

@Configuration
@ComponentScan("spring.course.soap")
public class SoapConfig {
    @Bean
    public SimpleJaxWsServiceExporter jaxWsExporter() {
        return new SimpleJaxWsServiceExporter();
    }
}
