package spring.course.soap.service;

import spring.course.soap.model.Book;

import java.util.List;

public interface IBookService {
    Long addBook(Book book);

    Book updateBook(Book book);

    void deleteBook(Long id);

    List<Book> books();

}
