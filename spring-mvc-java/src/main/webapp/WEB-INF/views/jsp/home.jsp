<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Book shop</title>
</head>


<body>
    <h1><s:message code="book.shop.welcome" /></h1>
    <a href="addBook">Add Book</a>
    <a href="viewBooks">View Books</a>

</body>