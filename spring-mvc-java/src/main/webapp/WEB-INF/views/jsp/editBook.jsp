<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>



<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Edit Book</h1>
<form:form method="POST" action="/spring-mvc-web-xml/editBook" commandName="book">
     <table >
         <tr>
            <td></td>
            <td><form:hidden  path="id" /></td>
         </tr>
         <tr>
            <td>Name : </td>
            <td><form:input path="name"  /><form:errors path="name" /></td>
          </tr>
          <tr>
            <td>Description :</td>
            <td><form:input path="description" /><form:errors path="description" /></td>
          </tr>
          <tr>
            <td>CountryCode :</td>
            <td><form:input path="countryCode" /><form:errors path="countryCode" /></td>
          </tr>
          <tr>
            <td>PAge numbers :</td>
            <td><form:input path="pageNumbers" /><form:errors path="pageNumbers" /></td>
          </tr>
          <tr>
            <td>Creation date :</td>
            <td><form:input path="creationDate" /></td>
          </tr>

          <tr>
            <td> </td>
            <td><input type="submit" value="Save" /></td>
          </tr>
     </table>
</form:form>