<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Add New Book</h1>
<form method="post" enctype="multipart/form-data">
     <table >
          <tr>
               <td>Name : </td>
               <td><input type="text" name="name"  /></td>
          </tr>
          <tr>
               <td>Description :</td>
               <td><input type="text" name="description"/></td>
          </tr>
          <tr>
                <td>Country code :</td>
                <td><input type="text" name="countryCode"/></td>
          </tr>
          <tr>
                <td>Creation date :</td>
                <td><input type="text" name="creationDate"/></td>
          </tr>
          <tr>
                <td>Page numbers :</td>
                <td><input type="text" name="pageNumbers"/></td>
          </tr>
          <tr>
                <td>Book Picture</label>:
                <td><input type="file" name="bookPicture" accept="image/jpeg,image/png,image/gif" /></td>
          </tr>
          <tr>
                <td>Author Picture</label>:
                <td><input type="file" name="authorPicture" accept="image/jpeg,image/png,image/gif" /></td>
          </tr>
          <tr>
               <td></td>
               <td><input type="submit" value="addBook"/></td>
          </tr>
     </table>


     <p>${hiddenMessage} </>
</form>