<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="en">
<head>
<title>Book shop</title>
</head>

<h1>Book List</h1>
<table border="2" width="70%" cellpadding="2">
    <tr><th>Id</th><th>Name</th><th>Description</th><th>CountryCode</th><th>CreationDate</th><th>First sale date</th><th>Edit</th><th>Delete</th><th>View book</th></tr>
       <c:forEach var="book" items="${books}">
           <tr>
           <td>${book.id}</td>
           <td>${book.name}</td>
           <td>${book.description}</td>
           <td>${book.countryCode}</td>
           <td>${book.creationDate}</td>
           <td>${book.firstSaleDate}</td>
           <td><a href="editBook/${book.id}">Edit</a></td>
           <td><a href="deleteBook/${book.id}">Delete</a></td>
           <td><a href="books?bookId=${book.id}">View book</a></td>
           </tr>
       </c:forEach>
       </table>
       <br/>
       <a href="addBook">Add Book</a>
       <a href="viewBooks">Back</a>