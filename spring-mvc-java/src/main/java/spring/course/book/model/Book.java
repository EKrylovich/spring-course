package spring.course.book.model;

import spring.course.validators.pagenumber.PageNumberConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Objects;

public class Book {
    private Long id;
    @NotNull
    @Size(max = 10, message = "{book.name.size.error}")
    private String name;
    @Size(min = 5, max = 30)
    private String description;
    @NotNull
    private String countryCode;
    @NotNull
    private LocalDate creationDate;

    private LocalDate firstSaleDate;
    @NotNull
    @PageNumberConstraint
    private Integer pageNumbers;

    public Book() {
    }

    public Book(Long id, String name, String description, String countryCode, LocalDate creationDate, Integer pageNumbers, LocalDate firstSaleDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.countryCode = countryCode;
        this.creationDate = creationDate;
        this.pageNumbers = pageNumbers;
        this.firstSaleDate = firstSaleDate;
    }

    public Book(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(Integer pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public LocalDate getFirstSaleDate() {
        return firstSaleDate;
    }

    public void setFirstSaleDate(LocalDate firstSaleDate) {
        this.firstSaleDate = firstSaleDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id) &&
                Objects.equals(name, book.name) &&
                Objects.equals(description, book.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
