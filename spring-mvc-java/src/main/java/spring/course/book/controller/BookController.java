package spring.course.book.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import spring.course.exceptions.BookNotFoundException;
import spring.course.exceptions.CannotDeleteException;
import spring.course.book.model.Book;
import spring.course.book.service.BookService;
import spring.course.exceptions.ValidationException;
import spring.course.tech.RequestBookName;

import javax.inject.Provider;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Collections;

@Controller
public class BookController {
    private final BookService bookService;
    private final Provider<RequestBookName> bookNameProvider;

    @Autowired
    public BookController(BookService bookService, Provider<RequestBookName> bookNameProvider) {
        this.bookService = bookService;
        this.bookNameProvider = bookNameProvider;
    }

    @RequestMapping(value = "viewBooks", method = RequestMethod.GET)
    public String books(Model model) {
        model.addAttribute("books", bookService.books());
        return "viewBooks";
    }

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public String books(Model model, @RequestParam Long bookId) {
        model.addAttribute("books", Collections.singleton(bookService.bookById(bookId)));
        return "viewBooks";
    }

    @RequestMapping(value = "/deleteBook/{bookId}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable Long bookId) {
        Book book = bookService.bookById(bookId);
        if (book == null)
            throw new CannotDeleteException("Book cannot be found");

        bookNameProvider.get().setName(book.getName());
        bookService.deleteBook(bookId);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public String addBook() {
        return "addBook";
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(@Validated Book book, Errors errors,
                          @RequestPart("bookPicture") byte[] bookPicture,
                          @RequestPart("authorPicture") MultipartFile authorPhoto) throws IOException {
        if (errors.hasErrors())
            throw new ValidationException(errors.toString());
        File file = new File(authorPhoto.getOriginalFilename());
        authorPhoto.transferTo(file);
        Book savedBook = bookService.addBook(book);
        return "redirect:/viewBooks";
    }

    @RequestMapping(value = "/editBook/{bookId}", method = RequestMethod.GET)
    public ModelAndView editBook(ModelAndView modelAndView, @PathVariable Long bookId) {
        if (bookService.bookById(bookId) == null)
            throw new BookNotFoundException();
        Book book = bookService.bookById(bookId);
//        modelAndView.addObject("book", book);
        modelAndView.addObject(book);
        modelAndView.setViewName("editBook");
        return modelAndView;
    }

    @RequestMapping(value = "/editBook", method = RequestMethod.POST)
    public String editBook(@Validated Book book, Errors errors) {
        if (errors.hasErrors())
            return "editBook";
        bookService.editBook(book);
        return "redirect:/viewBooks";
    }

    @ExceptionHandler(ValidationException.class)
    public String handleValidationException() {
        return "redirect:/addBook";
    }
}
