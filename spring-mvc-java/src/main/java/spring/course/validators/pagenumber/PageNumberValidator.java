package spring.course.validators.pagenumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PageNumberValidator implements ConstraintValidator<PageNumberConstraint, Integer> {
    @Override
    public void initialize(PageNumberConstraint pageNumberConstraint) {

    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext constraintValidatorContext) {
        return value > 0;
    }
}
