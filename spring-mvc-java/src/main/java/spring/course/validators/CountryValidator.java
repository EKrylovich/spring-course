package spring.course.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import spring.course.book.model.Book;

import java.util.Arrays;
import java.util.List;

@Component
public class CountryValidator implements Validator {
    private List<String> supportedCountryCodes = Arrays.asList("US", "UK", "BLR");


    @Override
    public boolean supports(Class<?> aClass) {
        return Book.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Book book = (Book) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "countryCode", "","Country code is empty");
        if (!supportedCountryCodes.contains(book.getCountryCode()))
            errors.rejectValue("countryCode", "CountryCode not supported");
    }
}
