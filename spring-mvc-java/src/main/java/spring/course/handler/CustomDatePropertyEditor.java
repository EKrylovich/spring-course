package spring.course.handler;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CustomDatePropertyEditor extends PropertyEditorSupport {
    public void setAsText(String value) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        setValue(LocalDate.parse(value, formatter));
    }

    public String getAsText() {
        String s = "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        if (getValue() != null) {
            s = formatter.format((LocalDate) getValue());
        }
        return s;
    }
}
