package spring.course.handler;

import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import spring.course.book.controller.BookController;
import spring.course.exceptions.CannotDeleteException;
import spring.course.validators.CountryValidator;

import java.time.LocalDate;

@ControllerAdvice(assignableTypes = {BookController.class})
public class ExceptionHandlers {

    private final CountryValidator countryValidator;

    public ExceptionHandlers(CountryValidator countryValidator) {
        this.countryValidator = countryValidator;
    }

    @ExceptionHandler(CannotDeleteException.class)
    public ModelAndView handleCannotDeleteException(CannotDeleteException exp){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("errorMessage", exp.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }


    @InitBinder("book")
    public void countryCodeBinding(WebDataBinder binder){
        binder.addValidators(countryValidator);
    }

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, new CustomDatePropertyEditor());
//        binder.addCustomFormatter(new CustomDateFormatter(), "firstSaleDate");
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("hiddenMessage", "Hidden message for spies");
    }


}
