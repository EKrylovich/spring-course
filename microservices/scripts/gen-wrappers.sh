#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}
set -e

cd ../core/book-service;                    note "Building book-service...";                      ./gradlew wrapper; cd -
cd ../core/recommendation-service;          note "Building recommendation-service...";            ./gradlew wrapper; cd -
cd ../core/book-compositor-service;         note "Building book-compositor-service...";           ./gradlew wrapper; cd -

cd ../support/auth-server;                  note "Building auth-server...";                       ./gradlew wrapper; cd -
cd ../support/discovery-service;            note "Building discovery-service...";                 ./gradlew wrapper; cd -
cd ../support/edge-server;                  note "Building edge-server...";                       ./gradlew wrapper; cd -
cd ../support/config-server;                note "Building config-server...";                     ./gradlew wrapper; cd -