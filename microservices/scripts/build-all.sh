#!/usr/bin/env bash

function note() {
    local GREEN NC
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color
    printf "\n${GREEN}$@  ${NC}\n" >&2
}
set -e

cd ../core/book-service;                    note "Building book-service...";                      ./gradlew clean build; cd -
cd ../core/recommendation-service;          note "Building recommendation-service...";            ./gradlew clean build; cd -
cd ../core/book-compositor-service;         note "Building book-compositor-service...";           ./gradlew clean build; cd -

cd ../support/auth-server;                  note "Building auth-server...";                       ./gradlew clean build; cd -
cd ../support/discovery-service;            note "Building discovery-service...";                 ./gradlew clean build; cd -
cd ../support/edge-server;                  note "Building edge-server...";                       ./gradlew clean build; cd -
cd ../support/config-server;                note "Building config-server...";                     ./gradlew clean build; cd -

find . -name *SNAPSHOT.jar -exec du -h {} \;

docker-compose build