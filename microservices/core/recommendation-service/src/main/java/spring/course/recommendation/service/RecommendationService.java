package spring.course.recommendation.service;

import org.springframework.stereotype.Service;
import spring.course.recommendation.model.Recommendation;
import spring.course.recommendation.repository.RecommendationRepository;

import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@Service
public class RecommendationService {
    private final RecommendationRepository recommendationRepository;

    public RecommendationService(RecommendationRepository recommendationRepository) {
        this.recommendationRepository = recommendationRepository;
    }

    public Long addRecommendation(Recommendation recommendation) {
        return recommendationRepository.save(recommendation).getId();
    }

    public Recommendation updateRecommendation(Recommendation book) {
        if (recommendationRepository.existsById(book.getId()))
            return recommendationRepository.save(book);
        return null;
    }

    public void deleteRecommendation(Long id) {
        recommendationRepository.deleteById(id);
    }

    public Recommendation findById(Long id){
        return recommendationRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Recommendation with id: " + id + "doesn't exist" ));
    }

    public List<Recommendation> recommendations(){
        return recommendationRepository.findAll();
    }

    public List<Recommendation> recommendationsForBook(Long bookId){
        return recommendationRepository.findByBookId(bookId);
    }
}
