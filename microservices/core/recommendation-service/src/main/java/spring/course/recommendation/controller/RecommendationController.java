package spring.course.recommendation.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import spring.course.recommendation.dto.RecommendationDto;
import spring.course.recommendation.service.RecommendationService;

import java.util.List;
import java.util.stream.Collectors;

import static spring.course.recommendation.RecommendationApplication.REST_API_VERSION;
import static spring.course.recommendation.dto.RecommendationDto.createDto;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@RestController
@RequestMapping(REST_API_VERSION)
public class RecommendationController {

    private final RecommendationService recommendationService;

    public RecommendationController(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }

    @GetMapping("recommendations")
    public List<RecommendationDto> recommendations(){
        return recommendationService.recommendations().stream()
                .map(RecommendationDto::createDto)
                .collect(Collectors.toList());
    }

    @GetMapping("recommendations/{id}")
    public RecommendationDto recommendations(@PathVariable Long id){
        return createDto(recommendationService.findById(id));
    }

    @GetMapping("recommendations/book/{bookId}")
    public List<RecommendationDto> recommendationsForBook(@PathVariable Long bookId){
        return recommendationService.recommendationsForBook(bookId).stream()
                .map(RecommendationDto::createDto)
                .collect(Collectors.toList());
    }

    @PostMapping("recommendations")
    public Long recommendation(final @Validated @RequestBody RecommendationDto recommendationDto){
        return recommendationService.addRecommendation(recommendationDto.map());
    }

    @DeleteMapping("recommendations/{id}")
    public void deleteRecommendation(@PathVariable Long id){
        recommendationService.deleteRecommendation(id);
    }

    @PutMapping("recommendations")
    public void updateRecommendation(@Validated @RequestBody RecommendationDto updateRecommendationDto){
        recommendationService.updateRecommendation(updateRecommendationDto.map());
    }

}
