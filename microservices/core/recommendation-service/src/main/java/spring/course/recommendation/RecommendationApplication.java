package spring.course.recommendation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import spring.course.recommendation.model.Recommendation;
import spring.course.recommendation.repository.RecommendationRepository;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableJpaRepositories
public class RecommendationApplication {


    public static final String REST_API_VERSION = "/api/v1/";

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RecommendationApplication.class);
        populateRecommendations(context);
    }

    private static void populateRecommendations(ApplicationContext context){
        RecommendationRepository recommendationRepository = context.getBean(RecommendationRepository.class);
        Recommendation recommendation = new Recommendation();
        recommendation.setTitle("Recommendation1");
        recommendation.setAuthor("Author1");
        recommendation.setBody("Good one1");
        recommendation.setBookId(1L);
        recommendationRepository.save(recommendation);
        Recommendation recommendation2 = new Recommendation();
        recommendation2.setTitle("Recommendation2");
        recommendation2.setAuthor("Author2");
        recommendation2.setBody("Good one2");
        recommendation2.setBookId(1L);
        recommendationRepository.save(recommendation2);
    }
}
