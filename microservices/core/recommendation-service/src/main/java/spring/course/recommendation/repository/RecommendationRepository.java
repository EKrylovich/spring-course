package spring.course.recommendation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.course.recommendation.model.Recommendation;

import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
public interface RecommendationRepository extends JpaRepository<Recommendation, Long>{
    List<Recommendation> findByBookId(Long bookId);
}
