package spring.course.compositor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.course.compositor.dto.BookDto;
import spring.course.compositor.dto.ComposedDto;
import spring.course.compositor.dto.RecommendationDto;
import spring.course.compositor.service.CompositorService;

import java.util.List;

import static spring.course.compositor.CompositorApplication.REST_API_VERSION;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@RestController
@RequestMapping(REST_API_VERSION)
public class CompositorController {

    private final CompositorService compositorService;

    public CompositorController(CompositorService compositorService) {
        this.compositorService = compositorService;
    }

    @GetMapping("book/{id}")
    public ComposedDto book(@PathVariable Long id){
        return compositorService.book(id);
    }
}
