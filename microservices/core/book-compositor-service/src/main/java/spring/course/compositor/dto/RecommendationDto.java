package spring.course.compositor.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
public class RecommendationDto {
    public final Long id;
    public final String title;
    public final String author;
    public final String body;
    public final Long bookId;

    @JsonCreator
    public RecommendationDto(@JsonProperty(value = "id") Long id,
                             @JsonProperty(value = "title") String title,
                             @JsonProperty(value = "author") String author,
                             @JsonProperty(value = "body") String body,
                             @JsonProperty(value = "bookId") Long bookId) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.body = body;
        this.bookId = bookId;
    }
}
