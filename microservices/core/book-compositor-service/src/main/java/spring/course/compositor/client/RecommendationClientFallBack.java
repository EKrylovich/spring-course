package spring.course.compositor.client;

import org.springframework.stereotype.Component;
import spring.course.compositor.dto.RecommendationDto;

import java.util.Collections;
import java.util.List;

/**
 * Created by ekrylovich
 * on 3/12/19.
 */
@Component
public class RecommendationClientFallBack implements RecommendationClient{
    @Override
    public List<RecommendationDto> recommendations(Long bookId) {
        return Collections.emptyList();
    }
}
