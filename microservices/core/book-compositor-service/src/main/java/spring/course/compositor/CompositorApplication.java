package spring.course.compositor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class CompositorApplication {


    public static final String REST_API_VERSION = "/api/v1/";

    public static void main(String[] args) {
        SpringApplication.run(CompositorApplication.class);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
