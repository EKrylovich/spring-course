package spring.course.compositor.client;

import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import spring.course.compositor.dto.RecommendationDto;

import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@FeignClient(name = "recommendation-service", fallback = RecommendationClientFallBack.class)
public interface RecommendationClient {

    @GetMapping("/api/v1/recommendations/book/{bookId}")
    List<RecommendationDto> recommendations(@PathVariable("bookId") Long bookId);
}
