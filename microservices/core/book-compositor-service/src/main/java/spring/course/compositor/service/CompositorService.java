package spring.course.compositor.service;

import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import spring.course.compositor.client.RecommendationClient;
import spring.course.compositor.dto.BookDto;
import spring.course.compositor.dto.ComposedDto;
import spring.course.compositor.dto.RecommendationDto;

import java.net.URI;
import java.util.Collections;
import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@Service
public class CompositorService {

    private final LoadBalancerClient loadBalancer;
    private final RestTemplate restTemplate = new RestTemplate();


    private final RecommendationClient recommendationClient;

    private final RestTemplate balancedRestTemplate;

    public CompositorService(LoadBalancerClient loadBalancer, RecommendationClient recommendationClient, RestTemplate balancedRestTemplate) {
        this.loadBalancer = loadBalancer;
        this.recommendationClient = recommendationClient;
        this.balancedRestTemplate = balancedRestTemplate;
    }

    public ComposedDto book(Long id) {
        ResponseEntity<BookDto> bookDto = getBook(id);
        List<RecommendationDto> recommendations = recommendationClient.recommendations(id);
        return ComposedDto.createDto(bookDto.getBody(), recommendations);
    }

    public ResponseEntity<BookDto> getBook(Long id) {
        return balancedRestTemplate.getForEntity("http://book-service/api/v1/books/" + id, BookDto.class);
    }

//    private ResponseEntity<BookDto> getBook(Long id) {
//        ServiceInstance instance = loadBalancer.choose("book-service");
//        URI uri = instance.getUri();
//        return restTemplate.getForEntity(uri.toString() + "api/v1/books/" + id, BookDto.class);
//    }
}
