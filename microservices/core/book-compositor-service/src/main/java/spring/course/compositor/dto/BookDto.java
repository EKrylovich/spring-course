package spring.course.compositor.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
public class BookDto {
    public final Long id;
    public final String name;
    public final String description;
//    public final List<RecommendationDto> recommendations;

    @JsonCreator
    public BookDto(@JsonProperty(value = "id") Long id,
                   @JsonProperty(value = "name", required = true) String name,
                   @JsonProperty(value = "description") String description) {
        this.id = id;
        this.name = name;
        this.description = description;
//        this.recommendations = recommendations;
    }

    public static BookDto createDtoWithRecommendation(final BookDto book){
        return new BookDto(book.id, book.name, book.description);
    }
}
