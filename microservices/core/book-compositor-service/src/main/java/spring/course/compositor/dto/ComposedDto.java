package spring.course.compositor.dto;

import java.util.List;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
public class ComposedDto {
    public final BookDto book;
    public final List<RecommendationDto> recommendations;

    private ComposedDto(final BookDto book, final List<RecommendationDto> recommendations) {
        this.book = book;
        this.recommendations = recommendations;
    }

    public static ComposedDto createDto(final BookDto book, final List<RecommendationDto> recommendations){
        return new ComposedDto(book, recommendations);
    }
}
