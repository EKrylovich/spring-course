package spring.course.book.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import spring.course.book.model.Book;

import java.util.*;

@Repository
public class BookRepository {

    private Map<Long, Book> storage = new HashMap<>();
    private static Long id = 0L;

    {
        storage.put(id, new Book(id,"FirstBook_Stored", "FirstDescription"));
        id++;
        storage.put(id, new Book(id,"SecondBook_Stored", "SecondDescription"));
        id++;
    }


    public Book save(Book book) {
        book.setId(id);
        storage.put(id, book);
        id++;
        return book;
    }

    public Book update(Book book) {
        return storage.put(book.getId(), book);
    }

    public boolean existById(Long id){
        return storage.containsKey(id);
    }

    public void deleteById(Long id) {
        storage.remove(id);
    }


    public Book findById(Long id) {
        return storage.get(id);
    }

    public List<Book> books(){
        return new ArrayList<>(storage.values());
    }
}
