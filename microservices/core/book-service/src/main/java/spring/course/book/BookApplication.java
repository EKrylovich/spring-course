package spring.course.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class BookApplication {

    public static final String REST_API_VERSION = "/api/v1/";

    public static void main(String[] args) {
        SpringApplication.run(BookApplication.class);
    }
}
