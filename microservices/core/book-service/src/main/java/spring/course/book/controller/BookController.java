package spring.course.book.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import spring.course.book.dto.BookDto;
import spring.course.book.service.BookService;

import java.util.List;
import java.util.stream.Collectors;

import static spring.course.book.BookApplication.REST_API_VERSION;
import static spring.course.book.dto.BookDto.createDto;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
@RestController
@RequestMapping(REST_API_VERSION)
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("books")
    public List<BookDto> books(){
        return bookService.books().stream()
                .map(BookDto::createDto)
                .collect(Collectors.toList());
    }

    @GetMapping("books/{id}")
    public BookDto books(@PathVariable Long id) throws InterruptedException {
        return createDto(bookService.findById(id));
    }

    @PostMapping("books")
    public Long book(final @Validated @RequestBody BookDto bookDto){
        return bookService.addBook(bookDto.map());
    }

    @DeleteMapping("book/{id}")
    public void deleteConnection(@PathVariable Long id){
        bookService.deleteBook(id);
    }

    @PutMapping("book")
    public void updateConnection(@Validated @RequestBody BookDto updateConnectionDto){
        bookService.updateBook(updateConnectionDto.map());
    }
}
