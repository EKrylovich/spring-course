package spring.course.book.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import spring.course.book.model.Book;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Created by ekrylovich
 * on 3/5/19.
 */
public class BookDto {
    public final Long id;
    @NotEmpty(message = "{book.name.empty}")
    @Size(max = 256, message = "{book.name.size}")
    public final String name;
    @Size(max = 1000, message = "{book.description.size}")
    public final String description;

    @JsonCreator
    public BookDto(@JsonProperty(value = "id") Long id,
                   @JsonProperty(value = "name", required = true) String name,
                   @JsonProperty(value = "description") String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Book map() {
        Book book = new Book();
        book.setId(id);
        book.setName(name);
        book.setDescription(description);
        return book;
    }


    public static BookDto createDto(final Book book){
        return new BookDto(book.getId(), book.getName(), book.getDescription());
    }
}
