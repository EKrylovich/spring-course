package spring.course.book.service;

import org.springframework.stereotype.Service;
import spring.course.book.model.Book;
import spring.course.book.repository.BookRepository;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Long addBook(Book book) {
        return bookRepository.save(book).getId();
    }

    public Book updateBook(Book book) {
        if (bookRepository.existById(book.getId()))
            return bookRepository.update(book);
        return null;
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public Book findById(Long id){
        return bookRepository.findById(id);
    }

    public List<Book> books(){
        return bookRepository.books();
    }
}
