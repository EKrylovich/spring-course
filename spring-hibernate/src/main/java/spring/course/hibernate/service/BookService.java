package spring.course.hibernate.service;

import org.springframework.stereotype.Service;
import spring.course.hibernate.model.Book;
import spring.course.hibernate.repository.BookRepository;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Long addBook(Book book) {
        return bookRepository.addBook(book);
    }

    public void updateBook(Book book) {
        bookRepository.updateBook(book);
    }

    public void deleteBook(Long id) {
        bookRepository.deleteBook(id);
    }

    public List<Book> books() {
        return bookRepository.books();
    }

    public void batchCreateBooks(List<Book> books){
        bookRepository.batchCreateBooks(books);
    }
}
