package spring.course.hibernate.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import spring.course.hibernate.model.Book;

import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

@Repository
public class BookRepository {
    private final SessionFactory sessionFactory;

    public BookRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    public Long addBook(Book book) {
        Serializable id = currentSession().save(book);
        return (Long) id;
    }

    @Transactional
    public void updateBook(Book book) {
        currentSession().update(book);
    }

    @Transactional
    public void deleteBook(Long id) {
        Book book = currentSession().load(Book.class, id);
        currentSession().delete(book);
    }

    @Transactional
    public List<Book> books() {
        Session session = currentSession();
        CriteriaQuery<Book> criteriaQuery = session.getCriteriaBuilder().createQuery(Book.class);
        criteriaQuery.from(Book.class);
        return session.createQuery(criteriaQuery).getResultList();
    }

    public void batchCreateBooks(List<Book> books){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            books.forEach(session::save);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            session.close();
        } finally {
            session.close();
        }
    }
}
