package spring.course.jpa.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import spring.course.jpa.model.Book;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class BookRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public Long addBook(Book book) {
        entityManager.persist(book);
        entityManager.flush();
        return book.getId();
    }

    public void updateBook(Book book) {
        entityManager.merge(book);
    }

    public void deleteBook(Long id) {
        Book book = entityManager.getReference(Book.class, id);
        entityManager.remove(book);
    }

    public List<Book> books() {
        return (List<Book>) entityManager.createQuery("Select t from " + Book.class.getSimpleName() + " t").getResultList();
    }

    public Book findById(Long id){
        return entityManager.find(Book.class, id);
    }
}
