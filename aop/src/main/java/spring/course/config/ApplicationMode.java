package spring.course.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class ApplicationMode {

    @Value("${configuration.debug}")
    private boolean isDebug;

    public boolean isDebug(){
        return isDebug;
    }
}
