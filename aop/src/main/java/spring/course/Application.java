package spring.course;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.book.controller.BookController;
import spring.course.book.model.Book;
import spring.course.config.AppConfiguration;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfiguration.class);
        BookController bookController = applicationContext.getBean(BookController.class);

        Book book = bookController.addBook("Sherlock Holmes", "Very interesting detective");
        System.out.println(book);
    }
}
