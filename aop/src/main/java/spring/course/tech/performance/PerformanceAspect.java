package spring.course.tech.performance;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import spring.course.config.ApplicationMode;
import spring.course.tech.Logger;

@Component
@Aspect
public class PerformanceAspect implements ApplicationContextAware {

    private final ApplicationMode applicationMode;
    private final Logger logger;

    private ApplicationContext applicationContext;

    public PerformanceAspect(ApplicationMode applicationMode, Logger logger) {
        this.applicationMode = applicationMode;
        this.logger = logger;
    }

    @Around("spring.course.tech.pointcuts.Pointcuts.controllerPointCut()")
    public Object measureController(ProceedingJoinPoint proceedingJoinPoint){
        if (applicationMode.isDebug()) {
            PerformanceTracker performanceTracker = applicationContext.getBean(PerformanceTracker.class);
            performanceTracker.startMeasure();
            try {
                Object object = proceedingJoinPoint.proceed();
                performanceTracker.endMeasure();
                logger.logMessage(proceedingJoinPoint + " call performance: " + performanceTracker.performance().toString());
                return object;
            } catch (Throwable throwable) {
                logger.logMessage(proceedingJoinPoint + " call failed: " + throwable.getMessage());
            }
        }
        return null;

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
