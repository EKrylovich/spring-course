package spring.course.tech.performance;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PerformanceTracker {
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public void startMeasure(){
        startTime = LocalDateTime.now();
    }

    public void endMeasure(){
        endTime = LocalDateTime.now();
    }

    public Duration performance() {
        return Duration.between(startTime, endTime);
    }
}
