package spring.course.tech.pointcuts;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Pointcuts {

    @Pointcut("execution(* spring.course.book.controller.*.*(..))")
    public void controllerPointCut(){};

    @Pointcut("within(spring.course.book.service..*)")
    public void servicePointCut(){};


    @Pointcut("within(spring.course.book.repository..*)")
    public void repositoryPointCut(){};
}
