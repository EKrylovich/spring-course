package spring.course.tech.transaction;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import spring.course.tech.Logger;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Transaction {
    private boolean started = false;
    private final Logger logger;

    public Transaction(Logger logger) {
        this.logger = logger;
    }

    public void startTransaction(){
        logger.logMessage("Transaction started");
        started = true;
    }

    public void finishTransaction(){
        logger.logMessage("Transaction finished");
        started = false;
    }

    public boolean isStarted(){
        return started;
    }
}
