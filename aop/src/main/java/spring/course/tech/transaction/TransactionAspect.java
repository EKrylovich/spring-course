package spring.course.tech.transaction;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import spring.course.tech.Logger;

@Component
@Aspect
public class TransactionAspect implements ApplicationContextAware {

    private ApplicationContext applicationContext;
    private final Logger logger;

    public TransactionAspect(Logger logger) {
        this.logger = logger;
    }

    @Around("spring.course.tech.pointcuts.Pointcuts.repositoryPointCut()")
    public void transactionSupport(ProceedingJoinPoint proceedingJoinPoint){

        Transaction transaction = applicationContext.getBean(Transaction.class);
        transaction.startTransaction();

        try {
            proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            logger.logMessage(proceedingJoinPoint + " call failed: " + throwable.getMessage());
        }

        transaction.finishTransaction();

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
