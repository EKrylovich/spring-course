package spring.course.tech;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.course.book.model.Book;
import spring.course.config.ApplicationMode;

@Component
@Aspect
public class Logger {
    private final ApplicationMode applicationMode;

    public Logger(ApplicationMode applicationMode) {
        this.applicationMode = applicationMode;
    }

    @Pointcut("execution(* spring.course.book.controller.*.*(..))")
    private void controllerPointCut(){};

    @Pointcut("within(spring.course.book.service..*)")
    private void servicePointCut(){};

    @Before("controllerPointCut()")
    public void logControllerBookName(JoinPoint joinPoint) {
        if (applicationMode.isDebug())
            logMessage("Call method " + joinPoint + "with book name " + joinPoint.getArgs()[0]);
    }

    @Before("servicePointCut() && args(book)")
    public void logServiceBookName(JoinPoint joinPoint, Book book) {
        if (applicationMode.isDebug())
            logMessage("Call method " + joinPoint + "with book name " + book.getName());
    }

    @AfterReturning(pointcut = "controllerPointCut() || servicePointCut()",
            returning = "book"
    )
    public void logMethod(JoinPoint joinPoint, Book book) {
        if (applicationMode.isDebug())
            logMessage("Call method " + joinPoint + " finished with book name " + book.getName());
    }

    public void logMessage(String message){
        System.out.println(message);
    }
}
