package spring.course.book.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import spring.course.book.model.Book;
import spring.course.book.repository.BookRepository;
import spring.course.tech.Logger;

@Component
//@PropertySource("classpath:application.properties")
public class BookService {
    private final BookRepository bookRepository;
//    private final Logger logger;

//    @Value("${configuration.debug}")
//    private boolean isDebug;

    public BookService(BookRepository bookRepository, Logger logger) {
        this.bookRepository = bookRepository;
//        this.logger = logger;
    }

    public Book addBook(Book book){
//        if (isDebug)
//            logger.logMessage("Call method BookService.addBook with name" + book.getName());

        Book savedBook = bookRepository.save(book);

//        if (isDebug)
//            logger.logMessage("Call method BookService.addBook with name" + book.getName() + "finished");

        return book;
    }
}
