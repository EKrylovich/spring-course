package spring.course.book.controller;

import org.springframework.stereotype.Component;
import spring.course.book.model.Book;
import spring.course.book.service.BookService;
import spring.course.config.ApplicationMode;
import spring.course.tech.Logger;

@Component
public class BookController {

    private final BookService bookService;
//    private final ApplicationMode applicationMode;
//    private final Logger logger;
//    private ApplicationContext applicationContext;

    public BookController(BookService bookService, ApplicationMode applicationMode, Logger logger) {
        this.bookService = bookService;
//        this.applicationMode = applicationMode;
//        this.logger = logger;
    }

    public Book addBook(String name, String description) {
//        PerformanceTracker performanceTracker = null;
//        if (applicationMode.isDebug()) {
//            logger.logMessage("Call method BookController.addBook with name" + name);
//            performanceTracker = applicationContext.getBean(PerformanceTracker.class);
//            performanceTracker.startMeasure();
//        }

        Book book = new Book();
        book.setName(name);
        book.setDescription(description);
        Book savedBook = bookService.addBook(book);

//        if (applicationMode.isDebug()) {
//            performanceTracker.endMeasure();
//            logger.logMessage("PerformanceTracker BookController.addBook with name " + name + ": " + performanceTracker.performance().toString());
//            logger.logMessage("Call method BookController.addBook with name" + name + " finished");
//        }
        return savedBook;
    }

//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//    }
}
