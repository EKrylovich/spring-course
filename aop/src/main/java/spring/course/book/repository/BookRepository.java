package spring.course.book.repository;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import spring.course.book.model.Book;
import spring.course.tech.transaction.Transaction;

import java.util.HashMap;
import java.util.Map;


@Component
public class BookRepository implements ApplicationContextAware {

    private static Long idCounter = 0L;

    private final Map<Long, Book> storage = new HashMap<>();
    private final Map<Long, Book> cache = new HashMap<>();

    private ApplicationContext applicationContext;

    public Book save(Book book){
//        Transaction transaction = applicationContext.getBean(Transaction.class);
//        transaction.startTransaction();

        book.setId(idCounter);
        storage.put(idCounter, book);
        idCounter++;

        cache.put(book.getId(), book);
//        transaction.finishTransaction();
        return book;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
