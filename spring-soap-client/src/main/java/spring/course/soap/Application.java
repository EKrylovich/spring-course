package spring.course.soap;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.course.soap.config.SoapClientConfig;
import spring.course.soap.model.Book;
import spring.course.soap.service.BookService;

import java.util.List;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SoapClientConfig.class);

        BookService bookService = applicationContext.getBean("bookService", BookService.class);
        System.out.println(bookService.books());
    }
}
