package spring.course.soap.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;
import spring.course.soap.service.BookService;

import java.net.MalformedURLException;
import java.net.URL;

@Configuration
public class SoapClientConfig {

    @Bean
    public JaxWsPortProxyFactoryBean bookService() throws MalformedURLException {
        JaxWsPortProxyFactoryBean proxy = new JaxWsPortProxyFactoryBean();
        proxy.setWsdlDocumentUrl(
                new URL("http://localhost:8080/BookWebService?wsdl"));
        proxy.setServiceName("BookWebService");
        proxy.setPortName("BookControllerPort");
        proxy.setServiceInterface(BookService.class);
        proxy.setNamespaceUri("http://controller.soap.course.spring/");
        return proxy;
    }
}
