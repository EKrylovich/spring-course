package spring.course.soap.service;

import spring.course.soap.model.Book;

import javax.jws.WebService;
import java.util.List;

@WebService(targetNamespace = "http://controller.soap.course.spring/")
public interface BookService {
    Long createBook(Book book);

    Book updateBook(Book book);

    void deleteBook(Long id);

    List<Book> books();

}
